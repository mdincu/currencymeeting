

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.currencymeeting.beans.Pirate;

public class MainTest {
	public static void main(String[] args) {
		
		ApplicationContext context = new AnnotationConfigApplicationContext("com.currencymeeting");
		
		Pirate pirate = context.getBean(Pirate.class);
		
		System.out.println(pirate.getTreasureMap());
		
	}
}


