package com.currencymeeting.beans;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Contact {
	private String name;
	private String country;
	private String city;
	@Column(name="phone_number")
	private String phoneNumber;

	public Contact(){}
	public Contact(String name, String country, String city, String phoneNumber){
		this.name = name;
		this.country = country;
		this.city = city;
		this.phoneNumber = phoneNumber;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
