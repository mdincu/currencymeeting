package com.currencymeeting.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@NamedQueries({
		@NamedQuery(name = "Transaction.findAllTransactions", query = "SELECT t FROM Transaction t WHERE t.firstCurrency=:firstCurrency AND t.secondCurrency=:secondCurrency"),
		@NamedQuery(name = "Transaction.findTransactionLT", query = "SELECT t FROM Transaction t WHERE t.firstCurrency=:firstCurrency AND t.secondCurrency=:secondCurrency AND t.secondAmount<=:secondAmount"),
		@NamedQuery(name = "Transaction.findTransactionGT", query = "SELECT t FROM Transaction t WHERE t.firstCurrency=:firstCurrency AND t.secondCurrency=:secondCurrency AND t.firstAmount>=:firstAmount"),
		@NamedQuery(name = "Transaction.findTransactionLTGT", query = "SELECT t FROM Transaction t WHERE t.firstCurrency=:firstCurrency AND t.secondCurrency=:secondCurrency AND t.firstAmount>=:firstAmount AND t.secondAmount<=:secondAmount"), })

@Entity
@Table(name="user_transactions")
public class Transaction implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "first_currency")
	private String firstCurrency;
	@Column(name = "first_amount")
	private long firstAmount;
	@Column(name = "second_currency")
	private String secondCurrency;
	@Column(name = "second_amount")
	private long secondAmount;
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Transaction() {
	}

	public Transaction(String firstCurrency, long firstAmount, String secondCurrency, long secondAmount) {
		this.firstCurrency = firstCurrency;
		this.firstAmount = firstAmount;
		this.secondCurrency = secondCurrency;
		this.secondAmount = secondAmount;
	}

	public String getFirstCurrency() {
		return firstCurrency;
	}

	public void setFirstCurrency(String firstCurrency) {
		this.firstCurrency = firstCurrency;
	}

	public long getFirstAmount() {
		return firstAmount;
	}

	public void setFirstAmount(long firstAmount) {
		this.firstAmount = firstAmount;
	}

	public String getSecondCurrency() {
		return secondCurrency;
	}

	public void setSecondCurrency(String secondCurrency) {
		this.secondCurrency = secondCurrency;
	}

	public long getSecondAmount() {
		return secondAmount;
	}

	public void setSecondAmount(long secondAmount) {
		this.secondAmount = secondAmount;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
