package com.currencymeeting.beans;

public class SessionId{
	private String clientId;
	private String clientIp;
	public SessionId(String clientId, String clientIp){
		this.clientId = clientId;
		this.clientIp = clientIp;
	}
	public String getClientId(){
		return clientId;
	}
	public String getClientIp(){
		return clientIp;
	}
	public boolean equals(Object object) {
		if (object == this)
			return true;
		if (object instanceof SessionId) {
			SessionId sessionId = (SessionId) object;
			if (sessionId.getClientId().equals(this.getClientId()) &&
				sessionId.getClientIp().equals(this.getClientIp()))
				return true;
		}
		return false;
	}
	public int hashCode(){
		return clientId.hashCode() ^ clientIp.hashCode();
	}
}
