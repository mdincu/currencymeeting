package com.currencymeeting.beans;

public class Session {

	private SessionId sessionId;
	private User user;
	private long sessionInitializationTime;

	public Session(String clientId, String clientIp, User user) {
		this.sessionId = new SessionId(clientId, clientIp);
		this.user = user;
		this.sessionInitializationTime = System.currentTimeMillis();
	}

	public SessionId getSessionId() {
		return sessionId;
	}

	public User getUser() {
		return user;
	}

	public long getSessionInitializationTime() {
		return sessionInitializationTime;
	}
	
	public void updateSession() {
		sessionInitializationTime = System.currentTimeMillis();
	}

	public boolean equals(Object object) {
		if (object == this)
			return true;
		if (object instanceof Session) {
			Session session = (Session) object;
			if (session.getUser().equals(this.getUser()))
				return true;
		}
		return false;
	}

	public int hashCode() {
		return user.hashCode();
	}
	
}
