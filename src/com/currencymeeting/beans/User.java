package com.currencymeeting.beans;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@NamedQueries({
		@NamedQuery(name = "User.login", query = "SELECT u FROM User u WHERE u.email=:email AND u.password=:password"),
		@NamedQuery(name = "User.findUserByEmail", query = "SELECT u FROM User u WHERE u.email=:email"), })
@Entity
@Table(name = "users")
@Component
@Scope(value = "prototype")
public class User implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(length = 40, nullable = false)
	private String email;
	@Column(length = 60, nullable = false)
	private String password;
	@Embedded
	private Contact contact;
	@OneToMany(mappedBy = "user")
	private Collection<Transaction> transactions;

	public User() {
	}

	public User(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Collection<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(Collection<Transaction> transactions) {
		this.transactions = transactions;
	}

}
