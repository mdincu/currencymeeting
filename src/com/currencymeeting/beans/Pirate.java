package com.currencymeeting.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;

@Component
public class Pirate {
	
	private TreasureMap treasureMap;

	public TreasureMap getTreasureMap() {
		return treasureMap;
	}
	@Autowired
	@Required
	public void setTreasureMap(TreasureMap treasureMap) {
		this.treasureMap = treasureMap;
	}

}
