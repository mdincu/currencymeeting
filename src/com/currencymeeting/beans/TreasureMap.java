package com.currencymeeting.beans;

import org.springframework.stereotype.Component;

@Component
public class TreasureMap{
	private String propertyInMap;

	public String getPropertyInMap() {
		return propertyInMap;
	}
	
	public void setPropertyInMap(String propertyInMap) {
		this.propertyInMap = " property in map";
	}
}
