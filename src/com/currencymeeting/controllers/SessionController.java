package com.currencymeeting.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.currencymeeting.beans.Session;
import com.currencymeeting.beans.SessionId;
import com.currencymeeting.beans.User;


@Component
@Scope(value="singleton")
public final class SessionController {

	private static final  Map<SessionId, Session> sessionList = new HashMap<>();
	private final long clearSessionInterval = TimeUnit.DAYS.toMillis(1);
	private long currentTime;
	private long sessionTime;
	private SessionController() {
		System.out.println("Session controller has been initialized");
		new ClearSession().start();
	}

	public void setSession(String clientId, String clientIp, User user) {
		Session session = new Session(clientId, clientIp, user);
		sessionList.put(session.getSessionId(), session);
	}
	
	public Map<SessionId, Session> getSessionList() {
		return sessionList;
	}
	
	public boolean checkConnection(String clientId, String clientIp){
		SessionId sessionId = new SessionId(clientId, clientIp);		
		boolean checkConnectionStatus = sessionList.containsKey(sessionId);
		if(checkConnectionStatus){
			Session session = sessionList.get(sessionId);
			session.updateSession();
		}
		return checkConnectionStatus;
	}
	
	public Session destroySession(String clientId, String clientIp){
		SessionId sessionId = new SessionId(clientId, clientIp);
		Session session = sessionList.remove(sessionId);
		return session;
	}
	
	public int getConnectionsNumber(){
		return sessionList.size();
	}
	
	private class ClearSession extends Thread{
		public void run(){
			try {
				while(true){
					currentTime = TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis());
					Set<SessionId> sessionListIds = sessionList.keySet();
					for(SessionId sessionId: sessionListIds){
						Session session = sessionList.get(sessionId);
						sessionTime = TimeUnit.MILLISECONDS.toDays(session.getSessionInitializationTime());
						if(currentTime-sessionTime>=1){
							sessionList.remove(sessionId);
						}
					}
					Thread.sleep(clearSessionInterval);
					System.out.println(getConnectionsNumber());
				}
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
		}
	}

}
