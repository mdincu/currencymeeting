package com.currencymeeting.controllers;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@Component
public class CurrencyRatesController {
	
	private JSONObject json = new JSONObject();
	
	private CurrencyRatesController() {
		new DownloadRates().start();
	}

	private static class Instance {
		private static CurrencyRatesController currencyRatesController = new CurrencyRatesController();
	}

	public static CurrencyRatesController getInstance() {
		return Instance.currencyRatesController;
	}

	public String getCurrencyRates() {
		return json.toString();
	}
	
	class DownloadRates extends Thread{
		public void run(){
			while(true){
				try{					
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					json.put("EUR", (double)1);
					DocumentBuilder builder = factory.newDocumentBuilder();
					Document doc = builder.parse("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
					NodeList cubeList = doc.getElementsByTagName("Cube");
					for (int i = 0; i < cubeList.getLength(); i++) {
						Node c = cubeList.item(i);
						if (c.getNodeType() == Node.ELEMENT_NODE) {
							Element cube = (Element) c;
							String currency = cube.getAttribute("currency");
							String rate = cube.getAttribute("rate");
							if (!currency.equals("") && !rate.equals("")) {
								json.put(currency, Double.parseDouble(rate));
							}
						}
					}
					Thread.sleep(TimeUnit.HOURS.toMillis(6));
				}catch(ParserConfigurationException pce){
					pce.printStackTrace();
				}catch(SAXException se){
					se.printStackTrace();
				}catch(JSONException je){
					je.printStackTrace();
				}catch(IOException ioe){
					ioe.printStackTrace();
				}catch(InterruptedException ie){
					ie.printStackTrace();
				}				
			}
		}
	}
}

/*
package com.currencymeeting.controllers;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class CurrencyController {
	
	private JSONArray currencies;
	
	private CurrencyController() {
		new DownloadRates().start();
	}

	private static class Instance {
		private static CurrencyController currencyRatesController = new CurrencyController();
	}

	public static CurrencyController getInstance() {
		return Instance.currencyRatesController;
	}

	public String getCurrencies() {
		return currencies.toString();
	}

	public void setCurrencies(JSONArray jsonArray) {
		this.currencies = jsonArray;
	}
	
	class DownloadRates extends Thread{
		public void run(){
			while(true){
				JSONObject jsonRates = null;
				try{
					jsonRates = new JSONObject();
					DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
					jsonRates.put("EUR", (double)1);
					DocumentBuilder builder = factory.newDocumentBuilder();
					Document doc = builder.parse("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
					NodeList cubeList = doc.getElementsByTagName("Cube");
					for (int i = 0; i < cubeList.getLength(); i++) {
						Node c = cubeList.item(i);
						if (c.getNodeType() == Node.ELEMENT_NODE) {
							Element cube = (Element) c;
							String currency = cube.getAttribute("currency");
							String rate = cube.getAttribute("rate");
							if (!currency.equals("") && !rate.equals("")) {
								jsonRates.put(currency, Double.parseDouble(rate));
							}
						}
					}
					String currenciesData = "[{\"Country\":\"Afghanistan\",\"Capital\":\"Kabul\",\"Currency Name\":\"Afghanistan Afghani\",\"Code\":\"AFN\"},{\"Country\":\"Albania\",\"Capital\":\"Tirana\",\"Currency Name\":\"Albanian Lek\",\"Code\":\"ALL\"},{\"Country\":\"Algeria\",\"Capital\":\"Algiers\",\"Currency Name\":\"Algerian Dinar\",\"Code\":\"DZD\"},{\"Country\":\"American Samoa\",\"Capital\":\"Pago Pago\",\"Currency Name\":\"US Dollar\",\"Code\":\"USD\"},{\"Country\":\"Andorra\",\"Capital\":\"Andorra\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Angola\",\"Capital\":\"Luanda\",\"Currency Name\":\"Angolan Kwanza\",\"Code\":\"AOA\"},{\"Country\":\"Anguilla\",\"Capital\":\"The Valley\",\"Currency Name\":\"East Caribbean Dollar\",\"Code\":\"XCD\"},{\"Country\":\"Antarctica\",\"Capital\":\"None\",\"Currency Name\":\"East Caribbean Dollar\",\"Code\":\"XCD\"},{\"Country\":\"Antigua and Barbuda\",\"Capital\":\"St. Johns\",\"Currency Name\":\"East Caribbean Dollar\",\"Code\":\"XCD\"},{\"Country\":\"Argentina\",\"Capital\":\"Buenos Aires\",\"Currency Name\":\"Argentine Peso\",\"Code\":\"ARS\"},{\"Country\":\"Armenia\",\"Capital\":\"Yerevan\",\"Currency Name\":\"Armenian Dram\",\"Code\":\"AMD\"},{\"Country\":\"Aruba\",\"Capital\":\"Oranjestad\",\"Currency Name\":\"Aruban Guilder\",\"Code\":\"AWG\"},{\"Country\":\"Australia\",\"Capital\":\"Canberra\",\"Currency Name\":\"Australian Dollar\",\"Code\":\"AUD\"},{\"Country\":\"Austria\",\"Capital\":\"Vienna\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Azerbaijan\",\"Capital\":\"Baku\",\"Currency Name\":\"Azerbaijan New Manat\",\"Code\":\"AZN\"},{\"Country\":\"Bahamas\",\"Capital\":\"Nassau\",\"Currency Name\":\"Bahamian Dollar\",\"Code\":\"BSD\"},{\"Country\":\"Bahrain\",\"Capital\":\"Al-Manamah\",\"Currency Name\":\"Bahraini Dinar\",\"Code\":\"BHD\"},{\"Country\":\"Bangladesh\",\"Capital\":\"Dhaka\",\"Currency Name\":\"Bangladeshi Taka\",\"Code\":\"BDT\"},{\"Country\":\"Barbados\",\"Capital\":\"Bridgetown\",\"Currency Name\":\"Barbados Dollar\",\"Code\":\"BBD\"},{\"Country\":\"Belarus\",\"Capital\":\"Minsk\",\"Currency Name\":\"Belarussian Ruble\",\"Code\":\"BYR\"},{\"Country\":\"Belgium\",\"Capital\":\"Brussels\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Belize\",\"Capital\":\"Belmopan\",\"Currency Name\":\"Belize Dollar\",\"Code\":\"BZD\"},{\"Country\":\"Benin\",\"Capital\":\"Porto-Novo\",\"Currency Name\":\"CFA Franc BCEAO\",\"Code\":\"XOF\"},{\"Country\":\"Bermuda\",\"Capital\":\"Hamilton\",\"Currency Name\":\"Bermudian Dollar\",\"Code\":\"BMD\"},{\"Country\":\"Bhutan\",\"Capital\":\"Thimphu\",\"Currency Name\":\"Bhutan Ngultrum\",\"Code\":\"BTN\"},{\"Country\":\"Bolivia\",\"Capital\":\"La Paz\",\"Currency Name\":\"Boliviano\",\"Code\":\"BOB\"},{\"Country\":\"Bosnia-Herzegovina\",\"Capital\":\"Sarajevo\",\"Currency Name\":\"Marka\",\"Code\":\"BAM\"},{\"Country\":\"Botswana\",\"Capital\":\"Gaborone\",\"Currency Name\":\"Botswana Pula\",\"Code\":\"BWP\"},{\"Country\":\"Bouvet Island\",\"Capital\":\"None\",\"Currency Name\":\"Norwegian Krone\",\"Code\":\"NOK\"},{\"Country\":\"Brazil\",\"Capital\":\"Brasilia\",\"Currency Name\":\"Brazilian Real\",\"Code\":\"BRL\"},{\"Country\":\"British Indian Ocean Territory\",\"Capital\":\"None\",\"Currency Name\":\"US Dollar\",\"Code\":\"USD\"},{\"Country\":\"Brunei Darussalam\",\"Capital\":\"Bandar Seri Begawan\",\"Currency Name\":\"Brunei Dollar\",\"Code\":\"BND\"},{\"Country\":\"Bulgaria\",\"Capital\":\"Sofia\",\"Currency Name\":\"Bulgarian Lev\",\"Code\":\"BGN\"},{\"Country\":\"Burkina Faso\",\"Capital\":\"Ouagadougou\",\"Currency Name\":\"CFA Franc BCEAO\",\"Code\":\"XOF\"},{\"Country\":\"Burundi\",\"Capital\":\"Bujumbura\",\"Currency Name\":\"Burundi Franc\",\"Code\":\"BIF\"},{\"Country\":\"Cambodia\",\"Capital\":\"Phnom Penh\",\"Currency Name\":\"Kampuchean Riel\",\"Code\":\"KHR\"},{\"Country\":\"Cameroon\",\"Capital\":\"Yaounde\",\"Currency Name\":\"CFA Franc BEAC\",\"Code\":\"XAF\"},{\"Country\":\"Canada\",\"Capital\":\"Ottawa\",\"Currency Name\":\"Canadian Dollar\",\"Code\":\"CAD\"},{\"Country\":\"Cape Verde\",\"Capital\":\"Praia\",\"Currency Name\":\"Cape Verde Escudo\",\"Code\":\"CVE\"},{\"Country\":\"Cayman Islands\",\"Capital\":\"Georgetown\",\"Currency Name\":\"Cayman Islands Dollar\",\"Code\":\"KYD\"},{\"Country\":\"Central African Republic\",\"Capital\":\"Bangui\",\"Currency Name\":\"CFA Franc BEAC\",\"Code\":\"XAF\"},{\"Country\":\"Chad\",\"Capital\":\"N'Djamena\",\"Currency Name\":\"CFA Franc BEAC\",\"Code\":\"XAF\"},{\"Country\":\"Chile\",\"Capital\":\"Santiago\",\"Currency Name\":\"Chilean Peso\",\"Code\":\"CLP\"},{\"Country\":\"China\",\"Capital\":\"Beijing\",\"Currency Name\":\"Yuan Renminbi\",\"Code\":\"CNY\"},{\"Country\":\"Christmas Island\",\"Capital\":\"The Settlement\",\"Currency Name\":\"Australian Dollar\",\"Code\":\"AUD\"},{\"Country\":\"Cocos (Keeling) Islands\",\"Capital\":\"West Island\",\"Currency Name\":\"Australian Dollar\",\"Code\":\"AUD\"},{\"Country\":\"Colombia\",\"Capital\":\"Bogota\",\"Currency Name\":\"Colombian Peso\",\"Code\":\"COP\"},{\"Country\":\"Comoros\",\"Capital\":\"Moroni\",\"Currency Name\":\"Comoros Franc\",\"Code\":\"KMF\"},{\"Country\":\"Congo\",\"Capital\":\"Brazzaville\",\"Currency Name\":\"CFA Franc BEAC\",\"Code\":\"XAF\"},{\"Country\":\"Congo, Dem. Republic\",\"Capital\":\"Kinshasa\",\"Currency Name\":\"Francs\",\"Code\":\"CDF\"},{\"Country\":\"Cook Islands\",\"Capital\":\"Avarua\",\"Currency Name\":\"New Zealand Dollar\",\"Code\":\"NZD\"},{\"Country\":\"Costa Rica\",\"Capital\":\"San Jose\",\"Currency Name\":\"Costa Rican Colon\",\"Code\":\"CRC\"},{\"Country\":\"Croatia\",\"Capital\":\"Zagreb\",\"Currency Name\":\"Croatian Kuna\",\"Code\":\"HRK\"},{\"Country\":\"Cuba\",\"Capital\":\"Havana\",\"Currency Name\":\"Cuban Peso\",\"Code\":\"CUP\"},{\"Country\":\"Cyprus\",\"Capital\":\"Nicosia\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Czech Rep.\",\"Capital\":\"Prague\",\"Currency Name\":\"Czech Koruna\",\"Code\":\"CZK\"},{\"Country\":\"Denmark\",\"Capital\":\"Copenhagen\",\"Currency Name\":\"Danish Krone\",\"Code\":\"DKK\"},{\"Country\":\"Djibouti\",\"Capital\":\"Djibouti\",\"Currency Name\":\"Djibouti Franc\",\"Code\":\"DJF\"},{\"Country\":\"Dominica\",\"Capital\":\"Roseau\",\"Currency Name\":\"East Caribbean Dollar\",\"Code\":\"XCD\"},{\"Country\":\"Dominican Republic\",\"Capital\":\"Santo Domingo\",\"Currency Name\":\"Dominican Peso\",\"Code\":\"DOP\"},{\"Country\":\"Ecuador\",\"Capital\":\"Quito\",\"Currency Name\":\"Ecuador Sucre\",\"Code\":\"ECS\"},{\"Country\":\"Egypt\",\"Capital\":\"Cairo\",\"Currency Name\":\"Egyptian Pound\",\"Code\":\"EGP\"},{\"Country\":\"El Salvador\",\"Capital\":\"San Salvador\",\"Currency Name\":\"El Salvador Colon\",\"Code\":\"SVC\"},{\"Country\":\"Equatorial Guinea\",\"Capital\":\"Malabo\",\"Currency Name\":\"CFA Franc BEAC\",\"Code\":\"XAF\"},{\"Country\":\"Eritrea\",\"Capital\":\"Asmara\",\"Currency Name\":\"Eritrean Nakfa\",\"Code\":\"ERN\"},{\"Country\":\"Estonia\",\"Capital\":\"Tallinn\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Ethiopia\",\"Capital\":\"Addis Ababa\",\"Currency Name\":\"Ethiopian Birr\",\"Code\":\"ETB\"},{\"Country\":\"European Union\",\"Capital\":\"Brussels\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Falkland Islands (Malvinas)\",\"Capital\":\"Stanley\",\"Currency Name\":\"Falkland Islands Pound\",\"Code\":\"FKP\"},{\"Country\":\"Faroe Islands\",\"Capital\":\"Torshavn\",\"Currency Name\":\"Danish Krone\",\"Code\":\"DKK\"},{\"Country\":\"Fiji\",\"Capital\":\"Suva\",\"Currency Name\":\"Fiji Dollar\",\"Code\":\"FJD\"},{\"Country\":\"Finland\",\"Capital\":\"Helsinki\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"France\",\"Capital\":\"Paris\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"French Guiana\",\"Capital\":\"Cayenne\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"French Southern Territories\",\"Capital\":\"None\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Gabon\",\"Capital\":\"Libreville\",\"Currency Name\":\"CFA Franc BEAC\",\"Code\":\"XAF\"},{\"Country\":\"Gambia\",\"Capital\":\"Banjul\",\"Currency Name\":\"Gambian Dalasi\",\"Code\":\"GMD\"},{\"Country\":\"Georgia\",\"Capital\":\"Tbilisi\",\"Currency Name\":\"Georgian Lari\",\"Code\":\"GEL\"},{\"Country\":\"Germany\",\"Capital\":\"Berlin\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Ghana\",\"Capital\":\"Accra\",\"Currency Name\":\"Ghanaian Cedi\",\"Code\":\"GHS\"},{\"Country\":\"Gibraltar\",\"Capital\":\"Gibraltar\",\"Currency Name\":\"Gibraltar Pound\",\"Code\":\"GIP\"},{\"Country\":\"Great Britain\",\"Capital\":\"London\",\"Currency Name\":\"Pound Sterling\",\"Code\":\"GBP\"},{\"Country\":\"Greece\",\"Capital\":\"Athens\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Greenland\",\"Capital\":\"Godthab\",\"Currency Name\":\"Danish Krone\",\"Code\":\"DKK\"},{\"Country\":\"Grenada\",\"Capital\":\"St. George's\",\"Currency Name\":\"East Carribean Dollar\",\"Code\":\"XCD\"},{\"Country\":\"Guadeloupe (French)\",\"Capital\":\"Basse-Terre\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Guam (USA)\",\"Capital\":\"Agana\",\"Currency Name\":\"US Dollar\",\"Code\":\"USD\"},{\"Country\":\"Guatemala\",\"Capital\":\"Guatemala City\",\"Currency Name\":\"Guatemalan Quetzal\",\"Code\":\"QTQ\"},{\"Country\":\"Guernsey\",\"Capital\":\"St. Peter Port\",\"Currency Name\":\"Pound Sterling\",\"Code\":\"GGP\"},{\"Country\":\"Guinea\",\"Capital\":\"Conakry\",\"Currency Name\":\"Guinea Franc\",\"Code\":\"GNF\"},{\"Country\":\"Guinea Bissau\",\"Capital\":\"Bissau\",\"Currency Name\":\"Guinea-Bissau Peso\",\"Code\":\"GWP\"},{\"Country\":\"Guyana\",\"Capital\":\"Georgetown\",\"Currency Name\":\"Guyana Dollar\",\"Code\":\"GYD\"},{\"Country\":\"Haiti\",\"Capital\":\"Port-au-Prince\",\"Currency Name\":\"Haitian Gourde\",\"Code\":\"HTG\"},{\"Country\":\"Heard Island and McDonald Islands\",\"Capital\":\"None\",\"Currency Name\":\"Australian Dollar\",\"Code\":\"AUD\"},{\"Country\":\"Honduras\",\"Capital\":\"Tegucigalpa\",\"Currency Name\":\"Honduran Lempira\",\"Code\":\"HNL\"},{\"Country\":\"Hong Kong\",\"Capital\":\"Victoria\",\"Currency Name\":\"Hong Kong Dollar\",\"Code\":\"HKD\"},{\"Country\":\"Hungary\",\"Capital\":\"Budapest\",\"Currency Name\":\"Hungarian Forint\",\"Code\":\"HUF\"},{\"Country\":\"Iceland\",\"Capital\":\"Reykjavik\",\"Currency Name\":\"Iceland Krona\",\"Code\":\"ISK\"},{\"Country\":\"India\",\"Capital\":\"New Delhi\",\"Currency Name\":\"Indian Rupee\",\"Code\":\"INR\"},{\"Country\":\"Indonesia\",\"Capital\":\"Jakarta\",\"Currency Name\":\"Indonesian Rupiah\",\"Code\":\"IDR\"},{\"Country\":\"Iran\",\"Capital\":\"Tehran\",\"Currency Name\":\"Iranian Rial\",\"Code\":\"IRR\"},{\"Country\":\"Iraq\",\"Capital\":\"Baghdad\",\"Currency Name\":\"Iraqi Dinar\",\"Code\":\"IQD\"},{\"Country\":\"Ireland\",\"Capital\":\"Dublin\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Isle of Man\",\"Capital\":\"Douglas\",\"Currency Name\":\"Pound Sterling\",\"Code\":\"GBP\"},{\"Country\":\"Israel\",\"Capital\":\"Jerusalem\",\"Currency Name\":\"Israeli New Shekel\",\"Code\":\"ILS\"},{\"Country\":\"Italy\",\"Capital\":\"Rome\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Ivory Coast\",\"Capital\":\"Abidjan\",\"Currency Name\":\"CFA Franc BCEAO\",\"Code\":\"XOF\"},{\"Country\":\"Jamaica\",\"Capital\":\"Kingston\",\"Currency Name\":\"Jamaican Dollar\",\"Code\":\"JMD\"},{\"Country\":\"Japan\",\"Capital\":\"Tokyo\",\"Currency Name\":\"Japanese Yen\",\"Code\":\"JPY\"},{\"Country\":\"Jersey\",\"Capital\":\"Saint Helier\",\"Currency Name\":\"Pound Sterling\",\"Code\":\"GBP\"},{\"Country\":\"Jordan\",\"Capital\":\"Amman\",\"Currency Name\":\"Jordanian Dinar\",\"Code\":\"JOD\"},{\"Country\":\"Kazakhstan\",\"Capital\":\"Astana\",\"Currency Name\":\"Kazakhstan Tenge\",\"Code\":\"KZT\"},{\"Country\":\"Kenya\",\"Capital\":\"Nairobi\",\"Currency Name\":\"Kenyan Shilling\",\"Code\":\"KES\"},{\"Country\":\"Kiribati\",\"Capital\":\"Tarawa\",\"Currency Name\":\"Australian Dollar\",\"Code\":\"AUD\"},{\"Country\":\"Korea-North\",\"Capital\":\"Pyongyang\",\"Currency Name\":\"North Korean Won\",\"Code\":\"KPW\"},{\"Country\":\"Korea-South\",\"Capital\":\"Seoul\",\"Currency Name\":\"Korean Won\",\"Code\":\"KRW\"},{\"Country\":\"Kuwait\",\"Capital\":\"Kuwait City\",\"Currency Name\":\"Kuwaiti Dinar\",\"Code\":\"KWD\"},{\"Country\":\"Kyrgyzstan\",\"Capital\":\"Bishkek\",\"Currency Name\":\"Som\",\"Code\":\"KGS\"},{\"Country\":\"Laos\",\"Capital\":\"Vientiane\",\"Currency Name\":\"Lao Kip\",\"Code\":\"LAK\"},{\"Country\":\"Latvia\",\"Capital\":\"Riga\",\"Currency Name\":\"Latvian Lats\",\"Code\":\"LVL\"},{\"Country\":\"Lebanon\",\"Capital\":\"Beirut\",\"Currency Name\":\"Lebanese Pound\",\"Code\":\"LBP\"},{\"Country\":\"Lesotho\",\"Capital\":\"Maseru\",\"Currency Name\":\"Lesotho Loti\",\"Code\":\"LSL\"},{\"Country\":\"Liberia\",\"Capital\":\"Monrovia\",\"Currency Name\":\"Liberian Dollar\",\"Code\":\"LRD\"},{\"Country\":\"Libya\",\"Capital\":\"Tripoli\",\"Currency Name\":\"Libyan Dinar\",\"Code\":\"LYD\"},{\"Country\":\"Liechtenstein\",\"Capital\":\"Vaduz\",\"Currency Name\":\"Swiss Franc\",\"Code\":\"CHF\"},{\"Country\":\"Lithuania\",\"Capital\":\"Vilnius\",\"Currency Name\":\"Lithuanian Litas\",\"Code\":\"LTL\"},{\"Country\":\"Luxembourg\",\"Capital\":\"Luxembourg\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Macau\",\"Capital\":\"Macau\",\"Currency Name\":\"Macau Pataca\",\"Code\":\"MOP\"},{\"Country\":\"Macedonia\",\"Capital\":\"Skopje\",\"Currency Name\":\"Denar\",\"Code\":\"MKD\"},{\"Country\":\"Madagascar\",\"Capital\":\"Antananarivo\",\"Currency Name\":\"Malagasy Franc\",\"Code\":\"MGF\"},{\"Country\":\"Malawi\",\"Capital\":\"Lilongwe\",\"Currency Name\":\"Malawi Kwacha\",\"Code\":\"MWK\"},{\"Country\":\"Malaysia\",\"Capital\":\"Kuala Lumpur\",\"Currency Name\":\"Malaysian Ringgit\",\"Code\":\"MYR\"},{\"Country\":\"Maldives\",\"Capital\":\"Male\",\"Currency Name\":\"Maldive Rufiyaa\",\"Code\":\"MVR\"},{\"Country\":\"Mali\",\"Capital\":\"Bamako\",\"Currency Name\":\"CFA Franc BCEAO\",\"Code\":\"XOF\"},{\"Country\":\"Malta\",\"Capital\":\"Valletta\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Marshall Islands\",\"Capital\":\"Majuro\",\"Currency Name\":\"US Dollar\",\"Code\":\"USD\"},{\"Country\":\"Martinique (French)\",\"Capital\":\"Fort-de-France\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Mauritania\",\"Capital\":\"Nouakchott\",\"Currency Name\":\"Mauritanian Ouguiya\",\"Code\":\"MRO\"},{\"Country\":\"Mauritius\",\"Capital\":\"Port Louis\",\"Currency Name\":\"Mauritius Rupee\",\"Code\":\"MUR\"},{\"Country\":\"Mayotte\",\"Capital\":\"Dzaoudzi\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Mexico\",\"Capital\":\"Mexico City\",\"Currency Name\":\"Mexican Nuevo Peso\",\"Code\":\"MXN\"},{\"Country\":\"Micronesia\",\"Capital\":\"Palikir\",\"Currency Name\":\"US Dollar\",\"Code\":\"USD\"},{\"Country\":\"Moldova\",\"Capital\":\"Kishinev\",\"Currency Name\":\"Moldovan Leu\",\"Code\":\"MDL\"},{\"Country\":\"Monaco\",\"Capital\":\"Monaco\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Mongolia\",\"Capital\":\"Ulan Bator\",\"Currency Name\":\"Mongolian Tugrik\",\"Code\":\"MNT\"},{\"Country\":\"Montenegro\",\"Capital\":\"Podgorica\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Montserrat\",\"Capital\":\"Plymouth\",\"Currency Name\":\"East Caribbean Dollar\",\"Code\":\"XCD\"},{\"Country\":\"Morocco\",\"Capital\":\"Rabat\",\"Currency Name\":\"Moroccan Dirham\",\"Code\":\"MAD\"},{\"Country\":\"Mozambique\",\"Capital\":\"Maputo\",\"Currency Name\":\"Mozambique Metical\",\"Code\":\"MZN\"},{\"Country\":\"Myanmar\",\"Capital\":\"Naypyidaw\",\"Currency Name\":\"Myanmar Kyat\",\"Code\":\"MMK\"},{\"Country\":\"Namibia\",\"Capital\":\"Windhoek\",\"Currency Name\":\"Namibian Dollar\",\"Code\":\"NAD\"},{\"Country\":\"Nauru\",\"Capital\":\"Yaren\",\"Currency Name\":\"Australian Dollar\",\"Code\":\"AUD\"},{\"Country\":\"Nepal\",\"Capital\":\"Kathmandu\",\"Currency Name\":\"Nepalese Rupee\",\"Code\":\"NPR\"},{\"Country\":\"Netherlands\",\"Capital\":\"Amsterdam\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Netherlands Antilles\",\"Capital\":\"Willemstad\",\"Currency Name\":\"Netherlands Antillean Guilder\",\"Code\":\"ANG\"},{\"Country\":\"New Caledonia (French)\",\"Capital\":\"Noumea\",\"Currency Name\":\"CFP Franc\",\"Code\":\"XPF\"},{\"Country\":\"New Zealand\",\"Capital\":\"Wellington\",\"Currency Name\":\"New Zealand Dollar\",\"Code\":\"NZD\"},{\"Country\":\"Nicaragua\",\"Capital\":\"Managua\",\"Currency Name\":\"Nicaraguan Cordoba Oro\",\"Code\":\"NIO\"},{\"Country\":\"Niger\",\"Capital\":\"Niamey\",\"Currency Name\":\"CFA Franc BCEAO\",\"Code\":\"XOF\"},{\"Country\":\"Nigeria\",\"Capital\":\"Lagos\",\"Currency Name\":\"Nigerian Naira\",\"Code\":\"NGN\"},{\"Country\":\"Niue\",\"Capital\":\"Alofi\",\"Currency Name\":\"New Zealand Dollar\",\"Code\":\"NZD\"},{\"Country\":\"Norfolk Island\",\"Capital\":\"Kingston\",\"Currency Name\":\"Australian Dollar\",\"Code\":\"AUD\"},{\"Country\":\"Northern Mariana Islands\",\"Capital\":\"Saipan\",\"Currency Name\":\"US Dollar\",\"Code\":\"USD\"},{\"Country\":\"Norway\",\"Capital\":\"Oslo\",\"Currency Name\":\"Norwegian Krone\",\"Code\":\"NOK\"},{\"Country\":\"Oman\",\"Capital\":\"Muscat\",\"Currency Name\":\"Omani Rial\",\"Code\":\"OMR\"},{\"Country\":\"Pakistan\",\"Capital\":\"Islamabad\",\"Currency Name\":\"Pakistan Rupee\",\"Code\":\"PKR\"},{\"Country\":\"Palau\",\"Capital\":\"Koror\",\"Currency Name\":\"US Dollar\",\"Code\":\"USD\"},{\"Country\":\"Panama\",\"Capital\":\"Panama City\",\"Currency Name\":\"Panamanian Balboa\",\"Code\":\"PAB\"},{\"Country\":\"Papua New Guinea\",\"Capital\":\"Port Moresby\",\"Currency Name\":\"Papua New Guinea Kina\",\"Code\":\"PGK\"},{\"Country\":\"Paraguay\",\"Capital\":\"Asuncion\",\"Currency Name\":\"Paraguay Guarani\",\"Code\":\"PYG\"},{\"Country\":\"Peru\",\"Capital\":\"Lima\",\"Currency Name\":\"Peruvian Nuevo Sol\",\"Code\":\"PEN\"},{\"Country\":\"Philippines\",\"Capital\":\"Manila\",\"Currency Name\":\"Philippine Peso\",\"Code\":\"PHP\"},{\"Country\":\"Pitcairn Island\",\"Capital\":\"Adamstown\",\"Currency Name\":\"New Zealand Dollar\",\"Code\":\"NZD\"},{\"Country\":\"Poland\",\"Capital\":\"Warsaw\",\"Currency Name\":\"Polish Zloty\",\"Code\":\"PLN\"},{\"Country\":\"Polynesia (French)\",\"Capital\":\"Papeete\",\"Currency Name\":\"CFP Franc\",\"Code\":\"XPF\"},{\"Country\":\"Portugal\",\"Capital\":\"Lisbon\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Puerto Rico\",\"Capital\":\"San Juan\",\"Currency Name\":\"US Dollar\",\"Code\":\"USD\"},{\"Country\":\"Qatar\",\"Capital\":\"Doha\",\"Currency Name\":\"Qatari Rial\",\"Code\":\"QAR\"},{\"Country\":\"Reunion (French)\",\"Capital\":\"Saint-Denis\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Romania\",\"Capital\":\"Bucharest\",\"Currency Name\":\"Romanian New Leu\",\"Code\":\"RON\"},{\"Country\":\"Russia\",\"Capital\":\"Moscow\",\"Currency Name\":\"Russian Ruble\",\"Code\":\"RUB\"},{\"Country\":\"Rwanda\",\"Capital\":\"Kigali\",\"Currency Name\":\"Rwanda Franc\",\"Code\":\"RWF\"},{\"Country\":\"Saint Helena\",\"Capital\":\"Jamestown\",\"Currency Name\":\"St. Helena Pound\",\"Code\":\"SHP\"},{\"Country\":\"Saint Kitts & Nevis Anguilla\",\"Capital\":\"Basseterre\",\"Currency Name\":\"East Caribbean Dollar\",\"Code\":\"XCD\"},{\"Country\":\"Saint Lucia\",\"Capital\":\"Castries\",\"Currency Name\":\"East Caribbean Dollar\",\"Code\":\"XCD\"},{\"Country\":\"Saint Pierre and Miquelon\",\"Capital\":\"St. Pierre\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Saint Vincent & Grenadines\",\"Capital\":\"Kingstown\",\"Currency Name\":\"East Caribbean Dollar\",\"Code\":\"XCD\"},{\"Country\":\"Samoa\",\"Capital\":\"Apia\",\"Currency Name\":\"Samoan Tala\",\"Code\":\"WST\"},{\"Country\":\"San Marino\",\"Capital\":\"San Marino\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Sao Tome and Principe\",\"Capital\":\"Sao Tome\",\"Currency Name\":\"Dobra\",\"Code\":\"STD\"},{\"Country\":\"Saudi Arabia\",\"Capital\":\"Riyadh\",\"Currency Name\":\"Saudi Riyal\",\"Code\":\"SAR\"},{\"Country\":\"Senegal\",\"Capital\":\"Dakar\",\"Currency Name\":\"CFA Franc BCEAO\",\"Code\":\"XOF\"},{\"Country\":\"Serbia\",\"Capital\":\"Belgrade\",\"Currency Name\":\"Dinar\",\"Code\":\"RSD\"},{\"Country\":\"Seychelles\",\"Capital\":\"Victoria\",\"Currency Name\":\"Seychelles Rupee\",\"Code\":\"SCR\"},{\"Country\":\"Sierra Leone\",\"Capital\":\"Freetown\",\"Currency Name\":\"Sierra Leone Leone\",\"Code\":\"SLL\"},{\"Country\":\"Singapore\",\"Capital\":\"Singapore\",\"Currency Name\":\"Singapore Dollar\",\"Code\":\"SGD\"},{\"Country\":\"Slovakia\",\"Capital\":\"Bratislava\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Slovenia\",\"Capital\":\"Ljubljana\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Solomon Islands\",\"Capital\":\"Honiara\",\"Currency Name\":\"Solomon Islands Dollar\",\"Code\":\"SBD\"},{\"Country\":\"Somalia\",\"Capital\":\"Mogadishu\",\"Currency Name\":\"Somali Shilling\",\"Code\":\"SOS\"},{\"Country\":\"South Africa\",\"Capital\":\"Pretoria\",\"Currency Name\":\"South African Rand\",\"Code\":\"ZAR\"},{\"Country\":\"South Georgia & South Sandwich Islands\",\"Capital\":\"None\",\"Currency Name\":\"Pound Sterling\",\"Code\":\"GBP\"},{\"Country\":\"South Sudan\",\"Capital\":\"Ramciel\",\"Currency Name\":\"South Sudan Pound\",\"Code\":\"SSP\"},{\"Country\":\"Spain\",\"Capital\":\"Madrid\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Sri Lanka\",\"Capital\":\"Colombo\",\"Currency Name\":\"Sri Lanka Rupee\",\"Code\":\"LKR\"},{\"Country\":\"Sudan\",\"Capital\":\"Khartoum\",\"Currency Name\":\"Sudanese Pound\",\"Code\":\"SDG\"},{\"Country\":\"Suriname\",\"Capital\":\"Paramaribo\",\"Currency Name\":\"Surinam Dollar\",\"Code\":\"SRD\"},{\"Country\":\"Svalbard and Jan Mayen Islands\",\"Capital\":\"Longyearbyen\",\"Currency Name\":\"Norwegian Krone\",\"Code\":\"NOK\"},{\"Country\":\"Swaziland\",\"Capital\":\"Mbabane\",\"Currency Name\":\"Swaziland Lilangeni\",\"Code\":\"SZL\"},{\"Country\":\"Sweden\",\"Capital\":\"Stockholm\",\"Currency Name\":\"Swedish Krona\",\"Code\":\"SEK\"},{\"Country\":\"Switzerland\",\"Capital\":\"Bern\",\"Currency Name\":\"Swiss Franc\",\"Code\":\"CHF\"},{\"Country\":\"Syria\",\"Capital\":\"Damascus\",\"Currency Name\":\"Syrian Pound\",\"Code\":\"SYP\"},{\"Country\":\"Taiwan\",\"Capital\":\"Taipei\",\"Currency Name\":\"Taiwan Dollar\",\"Code\":\"TWD\"},{\"Country\":\"Tajikistan\",\"Capital\":\"Dushanbe\",\"Currency Name\":\"Tajik Somoni\",\"Code\":\"TJS\"},{\"Country\":\"Tanzania\",\"Capital\":\"Dodoma\",\"Currency Name\":\"Tanzanian Shilling\",\"Code\":\"TZS\"},{\"Country\":\"Thailand\",\"Capital\":\"Bangkok\",\"Currency Name\":\"Thai Baht\",\"Code\":\"THB\"},{\"Country\":\"Togo\",\"Capital\":\"Lome\",\"Currency Name\":\"CFA Franc BCEAO\",\"Code\":\"XOF\"},{\"Country\":\"Tokelau\",\"Capital\":\"None\",\"Currency Name\":\"New Zealand Dollar\",\"Code\":\"NZD\"},{\"Country\":\"Tonga\",\"Capital\":\"Nuku'alofa\",\"Currency Name\":\"Tongan Pa'anga\",\"Code\":\"TOP\"},{\"Country\":\"Trinidad and Tobago\",\"Capital\":\"Port of Spain\",\"Currency Name\":\"Trinidad and Tobago Dollar\",\"Code\":\"TTD\"},{\"Country\":\"Tunisia\",\"Capital\":\"Tunis\",\"Currency Name\":\"Tunisian Dollar\",\"Code\":\"TND\"},{\"Country\":\"Turkey\",\"Capital\":\"Ankara\",\"Currency Name\":\"Turkish Lira\",\"Code\":\"TRY\"},{\"Country\":\"Turkmenistan\",\"Capital\":\"Ashgabat\",\"Currency Name\":\"Manat\",\"Code\":\"TMT\"},{\"Country\":\"Turks and Caicos Islands\",\"Capital\":\"Grand Turk\",\"Currency Name\":\"US Dollar\",\"Code\":\"USD\"},{\"Country\":\"Tuvalu\",\"Capital\":\"Funafuti\",\"Currency Name\":\"Australian Dollar\",\"Code\":\"AUD\"},{\"Country\":\"U.K.\",\"Capital\":\"London\",\"Currency Name\":\"Pound Sterling\",\"Code\":\"GBP\"},{\"Country\":\"Uganda\",\"Capital\":\"Kampala\",\"Currency Name\":\"Uganda Shilling\",\"Code\":\"UGX\"},{\"Country\":\"Ukraine\",\"Capital\":\"Kiev\",\"Currency Name\":\"Ukraine Hryvnia\",\"Code\":\"UAH\"},{\"Country\":\"United Arab Emirates\",\"Capital\":\"Abu Dhabi\",\"Currency Name\":\"Arab Emirates Dirham\",\"Code\":\"AED\"},{\"Country\":\"Uruguay\",\"Capital\":\"Montevideo\",\"Currency Name\":\"Uruguayan Peso\",\"Code\":\"UYU\"},{\"Country\":\"USA\",\"Capital\":\"Washington\",\"Currency Name\":\"US Dollar\",\"Code\":\"USD\"},{\"Country\":\"USA Minor Outlying Islands\",\"Capital\":\"None\",\"Currency Name\":\"US Dollar\",\"Code\":\"USD\"},{\"Country\":\"Uzbekistan\",\"Capital\":\"Tashkent\",\"Currency Name\":\"Uzbekistan Sum\",\"Code\":\"UZS\"},{\"Country\":\"Vanuatu\",\"Capital\":\"Port Vila\",\"Currency Name\":\"Vanuatu Vatu\",\"Code\":\"VUV\"},{\"Country\":\"Vatican\",\"Capital\":\"Vatican City\",\"Currency Name\":\"Euro\",\"Code\":\"EUR\"},{\"Country\":\"Venezuela\",\"Capital\":\"Caracas\",\"Currency Name\":\"Venezuelan Bolivar\",\"Code\":\"VEF\"},{\"Country\":\"Vietnam\",\"Capital\":\"Hanoi\",\"Currency Name\":\"Vietnamese Dong\",\"Code\":\"VND\"},{\"Country\":\"Virgin Islands (British)\",\"Capital\":\"Road Town\",\"Currency Name\":\"US Dollar\",\"Code\":\"USD\"},{\"Country\":\"Virgin Islands (USA)\",\"Capital\":\"Charlotte Amalie\",\"Currency Name\":\"US Dollar\",\"Code\":\"USD\"},{\"Country\":\"Wallis and Futuna Islands\",\"Capital\":\"Mata-Utu\",\"Currency Name\":\"CFP Franc\",\"Code\":\"XPF\"},{\"Country\":\"Western Sahara\",\"Capital\":\"El Aaiun\",\"Currency Name\":\"Moroccan Dirham\",\"Code\":\"MAD\"},{\"Country\":\"Yemen\",\"Capital\":\"San'a\",\"Currency Name\":\"Yemeni Rial\",\"Code\":\"YER\"},{\"Country\":\"Zambia\",\"Capital\":\"Lusaka\",\"Currency Name\":\"Zambian Kwacha\",\"Code\":\"ZMW\"},{\"Country\":\"Zimbabwe\",\"Capital\":\"Harare\",\"Currency Name\":\"Zimbabwe Dollar\",\"Code\":\"ZWD\"}]";
					JSONArray jsonArray = new JSONArray(currenciesData);
					JSONArray updatedCurrencies = new JSONArray();
					for(int i=0; i<jsonArray.length(); i++){
						JSONObject currency = jsonArray.getJSONObject(i);
						try {
							currency.put("Rate",jsonRates.get(currency.get("Code").toString()));
							updatedCurrencies.put(currency);
						} catch (JSONException je) {
							je.printStackTrace();
						}				
					}
					CurrencyController.this.setCurrencies(updatedCurrencies);
					Thread.sleep(TimeUnit.HOURS.toMillis(6));
				}catch(ParserConfigurationException pce){
					pce.printStackTrace();
				}catch(SAXException se){
					se.printStackTrace();
				}catch(JSONException je){
					je.printStackTrace();
				}catch(IOException ioe){
					ioe.printStackTrace();
				}catch(InterruptedException ie){
					ie.printStackTrace();
				}
			}
		}
	}

}
*/