package com.currencymeeting.controllers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public interface DatabaseConnection {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpaproject");
	public default EntityManager getEntityManager(){
		return emf.createEntityManager();
	}
}
