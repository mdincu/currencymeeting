package com.currencymeeting.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.currencymeeting.beans.Contact;
import com.currencymeeting.beans.Session;
import com.currencymeeting.beans.User;
import com.currencymeeting.dao.UserDAO;

@Path("/user")
public class UserController {
	
	private ApplicationContext context = new AnnotationConfigApplicationContext("com.currencymeeting");
	private SessionController sessionController = context.getBean(SessionController.class);
	
	@POST
	@Path("/create")
	public Response create(@FormParam("name") String name,
						   @FormParam("email") String email,
						   @FormParam("password") String password,
						   @FormParam("country") String country,
						   @FormParam("city") String city,
						   @FormParam("phoneNumber") String phoneNumber){
		
		boolean emailStatus = isEmail(email);
		
		if(!emailStatus){
			UserDAO userDao = new UserDAO();
			User user = new User(email, password);
			Contact contact = new Contact(name, country, city, phoneNumber);
			user.setContact(contact);
			boolean creationStatus = userDao.create(user);
			return Response.status(200).entity(creationStatus).build();
		}else{
			return Response.status(403).entity("error").build();
		}
		
	}
	
	@POST
	@Path("/checkemail")
	public Response checkEmail(@FormParam("email") String email){		
		boolean emailStatus = isEmail(email);
		if(emailStatus){
			return Response.status(200).entity(emailStatus).build();
		}else{
			return Response.status(403).entity("error").build();
		}
	}
	
	public boolean isEmail(String email){
		UserDAO userDao = new UserDAO();
		boolean emailStatus = userDao.findUserByEmail(email);
		return emailStatus;
	}
	
	@POST
	@Path("/login")
	public Response login(@Context HttpServletRequest request, @FormParam("email") String email, @FormParam("password") String password){		
		String clientId = request.getSession().getId();
		String clientIp = request.getRemoteAddr();
		UserDAO userDao = new UserDAO();
		User user = userDao.login(email, password);
		if(user!=null){
			sessionController.setSession(clientId, clientIp, user);
			String data = getData(user);
			return Response.status(200).entity(data).build();
		}else{
			return Response.status(403).entity("error").build();
		}
	}
	
	private String getData(User user){
		JSONObject jsonObject = new JSONObject();
		try {
			Contact contact = user.getContact();
			jsonObject.put("email", user.getEmail());
			jsonObject.put("name", contact.getName());
			jsonObject.put("country", contact.getCountry());
			jsonObject.put("city", contact.getCity());
			jsonObject.put("phoneNumber", contact.getPhoneNumber());
		} catch (JSONException je) {
			je.printStackTrace();
		}
		String data = jsonObject.toString();
		return data;		
	}
	
	@GET
	@Path("/connection")
	public Response checkConnection(@Context HttpServletRequest request){		
		String clientId = request.getSession().getId();
		String clientIp = request.getRemoteAddr();
		boolean connectionStatus = sessionController.checkConnection(clientId, clientIp);
		if(connectionStatus){
			return Response.status(200).entity("success").build();
		}else{
			return Response.status(403).entity("error").build();
		}
	}
	
	@GET
	@Path("/destroy")
	public Response destroy(@Context HttpServletRequest request){		
		String clientId = request.getSession().getId();
		String clientIp = request.getRemoteAddr();
		Session session = sessionController.destroySession(clientId, clientIp);
		if(session!=null){
			return Response.status(200).entity("success").build();
		}else{
			return Response.status(403).entity("error").build();
		}
	}
	
	@GET
	@Path("/rates")
	public Response getRates(){		
		CurrencyRatesController currencyRatesController = CurrencyRatesController.getInstance();
		String rates = currencyRatesController.getCurrencyRates();
		if(rates!=null){
			return Response.status(200).entity(rates).build();
		}else{
			return Response.status(403).entity("error").build();
		}
	}
	
}
