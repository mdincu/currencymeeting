package com.currencymeeting.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.stereotype.Component;

@Component
public class Connection {
	private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("currencymeeting");
	public static final EntityManager getEntityManager(){
		return emf.createEntityManager();
	}
}
