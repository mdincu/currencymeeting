package com.currencymeeting.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Component;

import com.currencymeeting.beans.Transaction;
import com.currencymeeting.beans.User;

@Component
public class TransactionDAO extends DAO<Transaction>{
		
	@SuppressWarnings("unchecked")
	public User login(String email, String password){
		EntityManager em = null;
		try{
			em=getEntityManager();
			Query q = em.createNamedQuery("User.login");
			q.setParameter("email", email);
			q.setParameter("password", password);
			List<User> userList = q.getResultList();
			if(userList.size()>0) return userList.get(0);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(em!=null){
				em.close();
			}
		}
		return null;
	}

}
