package com.currencymeeting.dao;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.currencymeeting.beans.User;

@Component
@Scope(value="singleton")
public class UserDAO extends DAO<User>{}
