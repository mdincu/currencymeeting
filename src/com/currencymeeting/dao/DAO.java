package com.currencymeeting.dao;

import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;

public class DAO<E>{

	private Class<E> type;
	
	@SuppressWarnings("unchecked")
	public DAO(){
		this.type = (Class<E>) ((ParameterizedType) getClass()
                	.getGenericSuperclass())
                	.getActualTypeArguments()[0];
	}
	
	public EntityManager getEntityManager(){
		return Connection.getEntityManager();
	}
	
	public boolean create(E entity){
		EntityManager em = null;
		try{
			em=getEntityManager();
			em.getTransaction().begin();
			em.persist(entity);
			em.getTransaction().commit();
			return true;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(em!=null){
				em.close();
			}
		}
		return false;
	}
	
	public boolean update(E entity){
		EntityManager em = null;
		try{
			em=getEntityManager();
			em.getTransaction().begin();
			em.merge(entity);
			em.getTransaction().commit();
			return true;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(em!=null){
				em.close();
			}
		}
		return false;
	}
	
	public boolean remove(int id){
		EntityManager em = null;
		try{
			em=getEntityManager();
			em.getTransaction().begin();
			E entity = em.getReference(type, id);
			em.remove(entity);
			em.getTransaction().commit();
			return true;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(em!=null){
				em.close();
			}
		}
		return false;
	}
	
}
