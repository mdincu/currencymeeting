package com.currencymeeting.dao;

import java.math.BigInteger;
import java.security.MessageDigest;

public class MD5 {

	private MessageDigest md;

	private static MD5 singleton;

	private MD5() {
	}

	public static MD5 getInstance() {
		if (singleton == null)
			singleton = new MD5();
		return singleton;
	}

	public String cryptMD5(String password) {
		try {
			md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(password.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			return hashtext;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
