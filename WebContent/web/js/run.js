app.run(function($rootScope){

	$rootScope.isLogged = !false;

	$rootScope.context = "has";
	$rootScope.labelCurrency1 = "I have:";
	$rootScope.labelCurrency2 = "I want:";

	$rootScope.changeContext = function(){
		var temp = $rootScope.labelCurrency1;
		$rootScope.labelCurrency1 = $rootScope.labelCurrency2;
		$rootScope.labelCurrency2 = temp;
		$rootScope.context = ($rootScope.context == "has") ? "wants" : "has";
		$(".context").addClass("text-glow");
		setTimeout(function(){			
			$(".context").removeClass("text-glow");
		}, 500);
	}

});