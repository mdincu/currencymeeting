app.config(function($routeProvider){

	$routeProvider
	.when('/search',{
		templateUrl: './search.html',
		controller: 'TransactionController'
	})
	.when('/new',{
		templateUrl: './new-transaction.html',
		controller: 'TransactionController'
	})
	.otherwise({
		templateUrl: './search.html',
		controller: 'TransactionController'
	});

});