app.controller('NavBarCtrl', function($scope,$location){
	(function(){
		$(".menu > li").removeClass('active');
		var path = '#' + $location.path().substring(1);
		$(".menu > li").find( "a[href='"+path+"']" ).parent().addClass('active');
	})();
	
	$(".menu > li").on('click',function(){
		$(".menu > li").removeAttr('class');
		$(this).addClass('active');	
	});
});