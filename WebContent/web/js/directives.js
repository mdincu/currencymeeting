app.directive('navBar',function(){
	return{		
		restrict: 'E',
		controller: 'NavBarCtrl',
		templateUrl: './navbar.html'
	}
});

app.directive('leftPanel',function(){
	return{		
		restrict: 'E',
		templateUrl: './left-panel.html'
	}
});

app.directive('rightPanel',function(){
	return{		
		restrict: 'E',
		templateUrl: './right-panel.html'
	}
});


app.directive('logStatusPanel',function(){
	return{		
		restrict: 'E',
		template: '<div ng-include="logStatusPanel"></div>',
		link: function($rootScope){
			if($rootScope.isLogged){
				$rootScope.logStatusPanel = "./logged-panel.html";
			}else{				
				$rootScope.logStatusPanel = "./unlogged-panel.html";
			}
		}
	}
});

app.directive('loginPanel',function(){
	return{		
		restrict: 'E',
		templateUrl: './login.html'
	}
});

app.directive('signupModal',function(){
	return{		
		restrict: 'E',
		templateUrl: './sign-up-modal.html'
	}
});

app.directive('signUpForm',function($rootScope){
	return{		
		restrict: 'E',
		template: '<div ng-include="signup">',
		link: function(){
			if($rootScope.isLogged){
				$rootScope.signup = "";
			}else{
				$rootScope.signup = "./sign-up-form.html";
			}
		}
	}
});

app.directive('countryCity',function($rootScope){
	return{		
		restrict: 'E',
		templateUrl: './country-city.html',
	}
});

/*app.directive('modelChange',function(){
	return{		
		restrict: 'ACE',
		link: function($scope){
			$scope.$watch('labelCurrency1',function(){
				console.log(this);
				if(this.last!=undefined)$(".context").css("background","red");
			});
		}
	}
});*/