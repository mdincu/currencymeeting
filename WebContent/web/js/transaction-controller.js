app.controller('TransactionController', function($scope,$log){
	
	//Search
	$(window).resize(function(){
		if(!$scope.isCollapsed) $('#map').attr('src',function(i,src){return src;});
	});
	
	$scope.reload = function(){
		if(!$scope.isCollapsed) $('#map').attr('src',function(i,src){return src;});
	}

	$scope.getLocation = function(val) {
		return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
			params: {
				address: val,
				sensor: false
			}
		}).then(function(response){
			return response.data.results.map(function(item){
				return item.formatted_address;
			});
		});
	};
	
	//New Transcation
	$scope.hstep = 1;
	$scope.mstep = 15;

	$scope.ismeridian = true;

	$scope.clear = function() {
		$scope.transactionTime = null;
	};

});