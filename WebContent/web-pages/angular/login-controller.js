app.controller("LoginController", function($rootScope, $scope, $compile,
										   $element, $mdSidenav, $location,
										   md5, cHttp, connection){
	
	this.checkEmail = function(){
		var loadingProgress = $compile("<loading></loading>")($scope);
		$('body').append(loadingProgress);		
		var data = {
			email: $scope.email
		};		
		cHttp.post("/currencymeeting/rest/user/checkemail", data)
	    .then(
    		function(success){
    	    	$scope.emailStatus="glyphicon glyphicon-ok form-control-feedback";
    	    	$scope.feedbackStatus="has-success has-feedback";
    	    	$('body > loading').remove();
    	    },
    	    function(error){
    	    	$scope.emailStatus="glyphicon glyphicon-remove form-control-feedback";
    	    	$scope.feedbackStatus="has-error has-feedback";
		    	$('body > loading').remove();
    	    }
	    );
	};
	
	this.login = function(){
		
		var loadingProgress = $compile("<loading></loading>")($scope);
		$('body').append(loadingProgress);		
		var data = {
			email: $scope.email,
			password: md5.crypt($scope.password)
		};		
		cHttp.post("/currencymeeting/rest/user/login", data)
	    .then(
    		function(success){
    	    	$rootScope.isConnection = true;
    	    	localStorage.user = JSON.stringify(success.data);
    	    	$rootScope.user = success.data;
    	    	//$mdSidenav('right-menu').toggle();
    	    	$('body > loading').remove();
    	    },
    	    function(error){
		    	connection.destroy();
		    	$('body > loading').remove();
    	    }
	    );
	};
	
	this.destroy = function(){
    	connection.destroy();
	};
	
});