app.directive('menu', function(){
   return {
       restrict: 'AEC',	       
       templateUrl: '/currencymeeting/web-pages/menu/menu.html?10'
   }
});

app.directive('connectionPanel', function($rootScope){
   return {
       restrict: 'AEC',
       link: function(){
    	   $rootScope.$watch('isConnection', function(){
    		   if($rootScope.isConnection === false){
    			   $rootScope.panel = "/currencymeeting/web-pages/menu/login-form.html";
    			}else if($rootScope.isConnection === true){
    				$rootScope.panel = "/currencymeeting/web-pages/menu/logged.html";
    			}    		   
    	   })
       },
       template: '<div ng-include="panel"></div>'
   }
});

app.directive('loading', function(){
   return {
       restrict: 'AEC',	       
       templateUrl: '/currencymeeting/web-pages/menu/loading.html?2'
   }
});