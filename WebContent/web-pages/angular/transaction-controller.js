app.controller('TransactionController', function($scope, $mdSidenav, cHttp){
	
	cHttp.get('http://ip-api.com/json/188.25.48.156')
	.then(
		function(success){			
			var json = success.data;
			$scope.$watch('countries', function(){
				$scope.country = json.country;
				$scope.getCities();
				$scope.city = json.city;
			});
		},
		function(error){
			//do nothing
		}
	);
	
	cHttp.get('/currencymeeting/web-pages/angular/countries/countries-json.txt')
	.then(
		function(success){			
			var json = success.data;
			var countries = [];
			for(var country in json){
				countries.push(country);
			}
			$scope.countries = countries;
			$scope.getCities = function(){
				$scope.cities = json[$scope.country];
			}
		},
		function(error){
			//do nothing
		}
	);
	
	cHttp.get('/currencymeeting/rest/user/rates')
	.then(
		function(success){
			var ratesJSON = success.data;
			cHttp.get('/currencymeeting/web-pages/angular/currencies/currencies.txt')
			.success(function(success){
				var currenciesJSON = success;
				var currencies = [];
				for(var jsonCurrencyCode in ratesJSON){
					for(var i=0; i<currenciesJSON.length; i++){
						var currencyObject = currenciesJSON[i];
						if(jsonCurrencyCode === currencyObject["Code"]){
							currencies.push(currencyObject);
							break;
						}
					}
				}
				currencies.sort();
				$scope.currencies = currencies;			
			});			 
		},
		function(error){
			//do nothing
		}
	);
	
	this.create = function(){
		
		var data = {
			currencyOne: $scope.currencyOne['Currency Name'],
			amountOne: $scope.amountOne,
			currencyTwo: $scope.currencyTwo['Currency Name'],
			amountTwo: $scope.amountTwo,
			time: $scope.time,
			place: $scope.place			
		}
		
		cHttp.post('/currencymeeting/rest/transaction/create', data)
		.then(
			function(success){			
				console.log(success);
			},
			function(error){
				//do nothing
			}
		);
	}
	
	this.search = function(){
		console.log($scope.country +", "+$scope.city +", "+$scope.currencyOne['Currency Name'] +", "+$scope.amountOne +", "+$scope.currencyTwo['Currency Name'] +", "+$scope.amountTwo);
	}
});