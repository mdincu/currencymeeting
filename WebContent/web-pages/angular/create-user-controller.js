app.controller("CreateUserController", function($rootScope, $scope, md5, cHttp){
	
	this.createUser = function(){		
		var data = {
			name: $scope.name,
			email: $scope.email,
			password: md5.crypt($scope.password),
			country: $scope.country,
			city: $scope.city,
			phoneNumber: $scope.phoneNumber,
		};
		cHttp.post("/currencymeeting/rest/user/create", data);	
	}
});