app.run(function($rootScope, $http, $document, cHttp, connection){	
	if(sessionStorage.connectionCheck !== "checked"){
		$http.get("/currencymeeting/rest/user/connection")
	    .then(
		    function(success){
		    	$rootScope.isConnection = true;
				$rootScope.user = JSON.parse(localStorage.user);
		    },
		    function(error){
		    	connection.destroy();
		    }
		);
		sessionStorage.connectionCheck = "checked";
	}else{
		if(localStorage.user!==undefined){
			$rootScope.isConnection = true;
			$rootScope.user = JSON.parse(localStorage.user);
		}else{
	    	connection.destroy();
		}
	}	
});